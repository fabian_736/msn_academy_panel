<?php

namespace App\Models\MsnAcademy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class User extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'msn.users';


    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }





    public function patient()
    {
        return $this->hasOne(Patient::class);
    }
}
