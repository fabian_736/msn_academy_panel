<?php

namespace App\Services\MsnAcademy;

use App\Models\MsnAcademy\User;


class UserService
{
    /**
     * @param mixed $request
     * 
     * @return  model
     */
    public static function save($request)
    {
        $user = new User();
        $user->name = $request->full_name;
        $user->email = $request->email;
        $user->avatar = $request->avatar;
        $user->role_id = $request->role_id;
        $user->active = $request->active;
        $user->password =  bcrypt($request->password);
        $user->save();
        return $user;
    }
}
