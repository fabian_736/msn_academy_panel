<?php

namespace App\Services\MsnAcademy\Professional;

use App\Models\MsnAcademy\Professional;
use App\Services\MsnAcademy\UserService;

class ProfessionalService
{

    /**
     * @param mixed $request
     * 
     * @return [type]
     */
    public static function save($request)
    {
        DB::$user = UserService::save($request);
        $professional = new Professional();
        $professional->first_name = $request->first_name;
        $professional->last_name = $request->last_name;
        $professional->address = $request->addres;
        $professional->phone =  $request->phone;
        $professional->description = $request->description;
        $professional->identification = $request->identification;
        $professional->speciality = $request->especiality;
        $professional->user_id = $user->id;
        $professional->save();
    }
}
