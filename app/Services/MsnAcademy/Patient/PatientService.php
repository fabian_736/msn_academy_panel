<?php

namespace App\Services\MsnAcademy\Patient;

use App\Models\MsnAcademy\Patient;
use App\Services\MsnAcademy\UserService;
use DB;

class PatientService
{

    /**
     * @param mixed $request
     * 
     * @return [type]
     */
    public static function save($request)
    {
        try {
            DB::beginTransaction();
            $user = UserService::save($request);
            $patient = new Patient();
            $patient->first_name = $request->full_name;
            $patient->last_name = $request->full_name;
            $patient->address = $request->address;
            $patient->phone =  $request->phone;
            $patient->description = $request->description;
            $patient->identification = $request->identification;
            $patient->speciality = $request->speciality;
            $patient->user_id = $user->id;
            $patient->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }
}
