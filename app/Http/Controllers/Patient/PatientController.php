<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MsnAcademy\User;

class PatientController extends Controller
{
    public function index()
    {
        $users = User::orderBy('users.created_at', 'Desc')->join('msn.patients', 'users.id', '=', 'patients.user_id')->where('role_id', 1)->get();
        return view('user.client', compact('users'));
    }

    public function show($id)
    {
        $user = User::find($id);

        return view('user.client', compact('user'));
    }

    public function store(Request $request)
    {

        $dd = 0;

        $dd = 0;

        return view('user.client', compact('user'));
    }
}
