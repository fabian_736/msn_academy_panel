<?php

namespace App\Http\Controllers\MsnAcademy;

use App\Http\Controllers\Controller;
use App\Http\Requests\MsnAcademy\User\StoreRequest;
use App\Services\MsnAcademy\Patient\PatientService;
use App\Services\MsnAcademy\Professional\ProfessionalService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //

    /**
     * @param StoreRequest $request
     * 
     * @return [type]
     */
    public function store(Request $request)
    {
        if ($request->role_id == 1) {
            $user = PatientService::save($request);
        }

        if ($request->role_id == 2) {
            $user = ProfessionalService::save($request);
        }

        if ($request->role_id == 3) {
        }
    }
}
