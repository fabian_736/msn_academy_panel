<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
class LoginController extends Controller
{
    protected $redirectTo = 'home';
    

    public function login_form(){
        return view('auth.form_login');
    }

    public function login(Request $request)
    {
        $credentials = $request->getCredentials();
        if(!Auth::validate($credentials)):
            return redirect()->to('login')
                ->withErrors(trans('auth.failed'));
        endif;
        $user = Auth::getProvider()->retrieveByCredentials($credentials);
        Auth::login($user);
        /** se verifica si ya leyo el tutorial */
        if(ReadTutorialService::get($user)){
            /** se accede a la vista del home usuario */
            return $this->home($user);
        }else{
           /** se envia al tutorial */
           return $this->tutorial($request, $user);
        }
    }
     
    public function logout() {       
         Auth::logout();
    }
}
