<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RutasController extends Controller
{


    // FLUJO AUTH (Login, Forgot, Reset, Code)

    public function auth_forgot()
    {
        return view('auth.forgot');
    }

    public function auth_code()
    {
        return view('auth.code');
    }

    public function auth_reset()
    {
        return view('auth.reset');
    }


    // FLUJO HOME

    public function portal()
    {
        return view('portal.index');
    }



    // FLUJO USUARIOS

    public function user_admin()
    {
        return view('user.admin');
    }

    public function user_client()
    {
        return view('user.client');
    }

    public function user_client_edit()
    {
        return view('user.client_edit');
    }


    public function user_admin_edit()
    {
        return view('user.admin_edit');
    }




    // FLUJO CAPACITACIONES (Modulos, Cursos, Cuestionarios)

    public function trainings_index()
    {
        return view('trainings.index');
    }

    public function trainings_list_courses()
    {
        return view('trainings.list_courses');
    }

    public function trainings_list_modules()
    {
        return view('trainings.list_modules');
    }



    // FLUJO CONTENIDOS (Noticias, recetas)

    public function contents_index()
    {
        return view('contents.index');
    }

    public function contents_list_news()
    {
        return view('contents.list_news');
    }

    public function contents_list_recipe()
    {
        return view('contents.list_recipe');
    }



    // FLUJO BLOG (Publicaciones, comentarios, interacciones)
    public function blog_publications()
    {
        return view('blog.publications');
    }

    public function blog_comments()
    {
        return view('blog.comments');
    }

    public function blog_interactions()
    {
        return view('blog.interactions');
    }



    // FLUJO PERFIL (Publicaciones, comentarios, interacciones)
    public function perfil()
    {
        return view('perfil.index');
    }
}