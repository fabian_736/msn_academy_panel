<?php

namespace App\Http\Requests\MsnAcademy\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'bail|required|max:100',
            'email' => 'bail|required|email',
            'password' => 'bail|required',
            'active' => 'bail|required|boolean',
            'addres' => 'bail|required|max:100',
            'phone' => 'bail|required|max:22',
            'role_id' => 'bail|required|numeric|in:1,2,3'
        ];
    }
}
