<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RutasController;
use App\Http\Controllers\Patient\PatientController;
use App\Http\Controllers\MsnAcademy\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
//Auth::routes();
Route::get('login/inicio', [LoginController::class, 'login_form'])->name('login.index');
//Route::get('login',[LoginController::class,'login_form'])->name('login');
//Route::post('login',[RutasController::class,'newpass'])->name('login');
Route::post('login/user', [LoginController::class, 'login'])->name('login.user')->middleware('guest');
Route::get('forgot', [RutasController::class, 'auth_forgot'])->name('auth.forgot');
Route::get('reset', [RutasController::class, 'reset'])->name('login.reset');
Route::post('send/code', [RutasController::class, 'sendCodeReset'])->name('send.code');
Route::get('send/code/user/{email}', [RutasController::class, 'sendCodeReset'])->name('resend.code');
Route::post('verify/code', [RutasController::class, 'verifyCodeChangePassword'])->name('verify.code');
Route::post('change/password', [RutasController::class, 'ChangePassword'])->name('change.password');


// Route::get('/forgot',[RutasController::class,'auth_forgot'])->name('auth.forgot');
// Route::get('/code',[RutasController::class,'auth_code'])->name('auth.code');
// Route::get('/reset',[RutasController::class,'auth_reset'])->name('auth.reset');


Route::get('/portal', [RutasController::class, 'portal'])->name('portal');


Route::get('/user/admin', [RutasController::class, 'user_admin'])->name('user.admin');
Route::get('/user/client', [PatientController::class, 'user_client'])->name('user.client');
Route::get('/user/admin/edit', [RutasController::class, 'user_admin_edit'])->name('user.admin_edit');
Route::get('/user/client/edit', [RutasController::class, 'user_client_edit'])->name('user.client_edit');
Route::post('/user/client/new', [UserController::class, 'store'])->name('user.new');


Route::get('/trainings/index', [RutasController::class, 'trainings_index'])->name('trainings.index');
Route::get('/trainings/list_courses', [RutasController::class, 'trainings_list_courses'])->name('trainings.list_courses');
Route::get('/trainings/list_modules', [RutasController::class, 'trainings_list_modules'])->name('trainings.list_modules');

Route::get('/contents/index', [RutasController::class, 'contents_index'])->name('contents.index');
Route::get('/contents/list_news', [RutasController::class, 'contents_list_news'])->name('contents.list_news');
Route::get('/contents/list_recipe', [RutasController::class, 'contents_list_recipe'])->name('contents.list_recipe');

Route::get('/blog/publications', [RutasController::class, 'blog_publications'])->name('blog.publications');
Route::get('/blog/comments', [RutasController::class, 'blog_comments'])->name('blog.comments');
Route::get('/blog/interactions', [RutasController::class, 'blog_interactions'])->name('blog.interactions');

Route::get('/perfil', [RutasController::class, 'perfil'])->name('perfil');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');