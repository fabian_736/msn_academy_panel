@extends('layouts.app')
@section('content')
    <div class="row-reverse">
        <div class="col my-5">
            <div class="row">
                <div class="col">
                    <label for="" class="h3">Recetas</label>
                </div>
            </div>
        </div>
        <div class="col">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th scope="col">Titulo</th>
                        <th scope="col">Noticia Enlazada</th>
                        <th scope="col">Responsable</th>
                        <th scope="col">Fecha caducidad</th>
                        <th scope="col">Documento</th>
                        <th scope="col">Banner</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Mark</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>
                            <div class="row ">
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50" data-bs-toggle="modal"
                                        data-bs-target="#exampleModal2"><i class="fas fa-edit"></i></a>
                                </div>
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Mark</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>
                            <div class="row ">
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50" data-bs-toggle="modal"
                                        data-bs-target="#exampleModal2"><i class="fas fa-edit"></i></a>
                                </div>
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Mark</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>
                            <div class="row ">
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50" data-bs-toggle="modal"
                                        data-bs-target="#exampleModal2"><i class="fas fa-edit"></i></a>
                                </div>
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>


        </div>
    </div>


    <!-- Modal Editar-->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar receta</h5>
                    <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body py-3">
                    <div class="row">
                        <div class="col">
                            <form action="" method="post">
                                <div class="row-reverse">
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <input type="text" name="" id="" class="form-control"
                                                    placeholder="Titulo de la receta">
                                            </div>
                                            <div class="col">
                                                <select name="" id="" class="form-control">
                                                    <option value="" disabled selected>Enlaza a la noticia...</option>
                                                    <option value="">Noticia - 1</option>
                                                    <option value="">Noticia - 2</option>
                                                    <option value="">Noticia - 3</option>
                                                    <option value="">Noticia - 4</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="col">
                                            <select name="" id="" class="form-control">
                                                <option value="" disabled selected>Responsable</option>
                                                <option value="">Super admin - 1</option>
                                                <option value="">Super admin - 2</option>
                                                <option value="">Super admin - 3</option>
                                                <option value="">Super admin - 4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <label for="">Fecha de vencimiento</label>
                                                <input type="date" name="" id="" class="form-control">
                                            </div>
                                            <div class="col d-flex align-items-center mt-auto">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value=""
                                                        id="fcustomCheck1" checked="">
                                                    <label class="custom-control-label" for="customCheck1">No tiene fecha
                                                        de vencimiento</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <label for="">Adjuntar documento o video</label>
                                                <input type="file" name="" id="" class="form-control">
                                            </div>
                                            <div class="col">
                                                <label for="">Adjuntar imagen del banner</label>
                                                <input type="file" name="" id="" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center ">
                    <a href="javascript:;" class="btn w-50 bg-gradient-primary">ACTUALIZAR</a>
                </div>
            </div>
        </div>
    </div>
@endsection
