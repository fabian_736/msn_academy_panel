@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col">
            <div class="card card-body">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center my-3">
                        <div class="row">
                            <div class="col d-flex justify-content-center align-items-center">
                                <label for="" class="h3">Noticias</label>
                            </div>
                            <div class="col d-flex justify-content-center align-items-center">
                                <a href="javascript:;" style="cursor:pointer" title="Agregar Modulo" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal">
                                    <i class="fas fa-plus-circle fa-2x mb-1" style="color: #312783 "></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col d-flex justify-content-center ">
                            <a href="{{ route('contents.list_news') }}" class="h5"
                                style="text-decoration-line: underline">Ver Mas</a>
                        </div>
                    </div>
                    <div class="col ">
                        <div class="row-reverse ">
                            <div class="col-10 mx-auto card_list mb-3">
                                <div class="card ">
                                    <div class="img_card p-3">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="row-reverse">
                                                    <div class="col">
                                                        <p>Titulo</p>
                                                    </div>
                                                    <div class="col" style="max-height: 5vh; overflow: auto">
                                                        <p class="fa-sm">Lorem ipsum dolor, sit amet consectetur
                                                            adipisicing elit.
                                                            Explicabo, alias praesentium error sed iusto qui consectetur
                                                            nostrum dolore quae temporibus ad fugit exercitationem
                                                            voluptatum placeat perferendis eaque dolorum soluta asperiores.
                                                        </p>
                                                    </div>
                                                    <div class="col">
                                                        <p>Numero de cursos: #</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col d-flex justify-content-center align-items-center">
                                                <div class="img_curso">
                                                    <img src="{{ url('img/user2.png') }}" alt="" class="w-100">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-10 mx-auto card_list mb-3">
                                <div class="card ">
                                    <div class="img_card p-3">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="row-reverse">
                                                    <div class="col">
                                                        <p>Titulo</p>
                                                    </div>
                                                    <div class="col" style="max-height: 5vh; overflow: auto">
                                                        <p class="fa-sm">Lorem ipsum dolor, sit amet consectetur
                                                            adipisicing elit.
                                                            Explicabo, alias praesentium error sed iusto qui consectetur
                                                            nostrum dolore quae temporibus ad fugit exercitationem
                                                            voluptatum placeat perferendis eaque dolorum soluta asperiores.
                                                        </p>
                                                    </div>
                                                    <div class="col">
                                                        <p>Numero de cursos: #</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col d-flex justify-content-center align-items-center">
                                                <div class="img_curso">
                                                    <img src="{{ url('img/user2.png') }}" alt="" class="w-100">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-10 mx-auto card_list mb-3">
                                <div class="card ">
                                    <div class="img_card p-3">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="row-reverse">
                                                    <div class="col">
                                                        <p>Titulo</p>
                                                    </div>
                                                    <div class="col" style="max-height: 5vh; overflow: auto">
                                                        <p class="fa-sm">Lorem ipsum dolor, sit amet consectetur
                                                            adipisicing elit.
                                                            Explicabo, alias praesentium error sed iusto qui consectetur
                                                            nostrum dolore quae temporibus ad fugit exercitationem
                                                            voluptatum placeat perferendis eaque dolorum soluta asperiores.
                                                        </p>
                                                    </div>
                                                    <div class="col">
                                                        <p>Numero de cursos: #</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col d-flex justify-content-center align-items-center">
                                                <div class="img_curso">
                                                    <img src="{{ url('img/user2.png') }}" alt="" class="w-100">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card card-body">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center my-3">
                        <div class="row">
                            <div class="col d-flex justify-content-center align-items-center">
                                <label for="" class="h3">Recetas</label>
                            </div>
                            <div class="col d-flex justify-content-center align-items-center">
                                <a href="javascript:;" style="cursor:pointer" title="Agregar Modulo" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal2">
                                    <i class="fas fa-plus-circle fa-2x mb-1" style="color: #312783 "></i>
                                </a>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col d-flex justify-content-center ">
                            <a href="{{ route('contents.list_recipe') }}" class="h5"
                                style="text-decoration-line: underline">Ver Mas</a>
                        </div>
                    </div>
                    <div class="col ">
                        <div class="row-reverse ">
                            <div class="col-10 mx-auto card_list mb-3">
                                <div class="card ">
                                    <div class="img_card p-3">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="row-reverse">
                                                    <div class="col">
                                                        <p>Titulo</p>
                                                    </div>
                                                    <div class="col" style="max-height: 5vh; overflow: auto">
                                                        <p class="fa-sm">Lorem ipsum dolor, sit amet consectetur
                                                            adipisicing elit.
                                                            Explicabo, alias praesentium error sed iusto qui consectetur
                                                            nostrum dolore quae temporibus ad fugit exercitationem
                                                            voluptatum placeat perferendis eaque dolorum soluta asperiores.
                                                        </p>
                                                    </div>
                                                    <div class="col">
                                                        <p>Numero de cursos: #</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col d-flex justify-content-center align-items-center">
                                                <div class="img_curso">
                                                    <img src="{{ url('img/user2.png') }}" alt="" class="w-100">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-10 mx-auto card_list mb-3">
                                <div class="card ">
                                    <div class="img_card p-3">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="row-reverse">
                                                    <div class="col">
                                                        <p>Titulo</p>
                                                    </div>
                                                    <div class="col" style="max-height: 5vh; overflow: auto">
                                                        <p class="fa-sm">Lorem ipsum dolor, sit amet consectetur
                                                            adipisicing elit.
                                                            Explicabo, alias praesentium error sed iusto qui consectetur
                                                            nostrum dolore quae temporibus ad fugit exercitationem
                                                            voluptatum placeat perferendis eaque dolorum soluta asperiores.
                                                        </p>
                                                    </div>
                                                    <div class="col">
                                                        <p>Numero de cursos: #</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col d-flex justify-content-center align-items-center">
                                                <div class="img_curso">
                                                    <img src="{{ url('img/user2.png') }}" alt="" class="w-100">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-10 mx-auto card_list mb-3">
                                <div class="card ">
                                    <div class="img_card p-3">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="row-reverse">
                                                    <div class="col">
                                                        <p>Titulo</p>
                                                    </div>
                                                    <div class="col" style="max-height: 5vh; overflow: auto">
                                                        <p class="fa-sm">Lorem ipsum dolor, sit amet consectetur
                                                            adipisicing elit.
                                                            Explicabo, alias praesentium error sed iusto qui consectetur
                                                            nostrum dolore quae temporibus ad fugit exercitationem
                                                            voluptatum placeat perferendis eaque dolorum soluta asperiores.
                                                        </p>
                                                    </div>
                                                    <div class="col">
                                                        <p>Numero de cursos: #</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col d-flex justify-content-center align-items-center">
                                                <div class="img_curso">
                                                    <img src="{{ url('img/user2.png') }}" alt="" class="w-100">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Modulos-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear noticias</h5>
                    <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body py-3">
                    <div class="row">
                        <div class="col">
                            <form action="" method="post">
                                <div class="row-reverse">
                                    <div class="col mb-3">
                                        <input type="text" name="" id="" class="form-control"
                                            placeholder="Titulo de la noticia">
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <textarea name="" id="" cols="10" rows="5" class="form-control" placeholder="Descripcion de la noticia"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <select name="" id="" class="form-control">
                                            <option value="" disabled selected>Responsable</option>
                                            <option value="">Super admin - 1</option>
                                            <option value="">Super admin - 2</option>
                                            <option value="">Super admin - 3</option>
                                            <option value="">Super admin - 4</option>
                                        </select>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <label for="">Fecha de vencimiento</label>
                                                <input type="date" name="" id="" class="form-control">
                                            </div>
                                            <div class="col d-flex align-items-center mt-auto">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value=""
                                                        id="fcustomCheck1" checked="">
                                                    <label class="custom-control-label" for="customCheck1">No tiene fecha
                                                        de vencimiento</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <select name="" id="" class="form-control">
                                                    <option value="" disabled selected>Seleccione la categoria</option>
                                                    <option value="">Categoria - 1</option>
                                                    <option value="">Categoria - 2</option>
                                                    <option value="">Categoria - 3</option>
                                                    <option value="">Categoria - 4</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center ">
                    <a href="javascript:;" class="btn w-50 bg-gradient-primary">REGISTRAR</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Cursos-->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear receta</h5>
                    <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body py-3">
                    <div class="row">
                        <div class="col">
                            <form action="" method="post">
                                <div class="row-reverse">
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <input type="text" name="" id="" class="form-control"
                                                    placeholder="Titulo de la receta">
                                            </div>
                                            <div class="col">
                                                <select name="" id="" class="form-control">
                                                    <option value="" disabled selected>Enlaza a la noticia...</option>
                                                    <option value="">Noticia - 1</option>
                                                    <option value="">Noticia - 2</option>
                                                    <option value="">Noticia - 3</option>
                                                    <option value="">Noticia - 4</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="col">
                                            <select name="" id="" class="form-control">
                                                <option value="" disabled selected>Responsable</option>
                                                <option value="">Super admin - 1</option>
                                                <option value="">Super admin - 2</option>
                                                <option value="">Super admin - 3</option>
                                                <option value="">Super admin - 4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <label for="">Fecha de vencimiento</label>
                                                <input type="date" name="" id="" class="form-control">
                                            </div>
                                            <div class="col d-flex align-items-center mt-auto">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value=""
                                                        id="fcustomCheck1" checked="">
                                                    <label class="custom-control-label" for="customCheck1">No tiene fecha
                                                        de vencimiento</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <label for="">Adjuntar documento o video</label>
                                                <input type="file" name="" id="" class="form-control">
                                            </div>
                                            <div class="col">
                                                <label for="">Adjuntar imagen del banner</label>
                                                <input type="file" name="" id="" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center ">
                    <a href="javascript:;" class="btn w-50 bg-gradient-primary">REGISTRAR</a>
                </div>
            </div>
        </div>
    </div>


    <style>
        .card_list {
            min-height: 20vh;
            max-height: 20vh;
            background: #F5F5F5;
            border-radius: 20px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .img_card {
            min-height: 15vh;
            max-height: 15vh;
            max-width: 30vw;
            min-width: 30vw;
            border-radius: 20px;
            cursor: pointer;
            overflow: hidden;
        }

        .img_card:hover {
            border-color: #312783;
            border-style: solid;
            border-width: 2px;
        }

        .img_curso {
            width: 100px;
            height: 100px;
            background: gray;
            border-radius: 60px;
        }

    </style>
@endsection
