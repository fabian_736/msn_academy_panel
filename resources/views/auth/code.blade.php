@extends('layouts.auth.app')
@section('content_auth')
    <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
            <div class="card card-plain mt-8">
                <div class="card-header pb-0 text-left bg-transparent">
                    <img src="{{ url('img/logo/1.png') }}" alt="" class="w-100">
                    <p class="mb-0">Por digita el codigo que llego a tu correo</p>
                </div>
                <div class="card-body">
                    <form role="form">
                        <label>Escribir codigo</label>
                        <div class="mb-3">
                            <input type="number" class="form-control" placeholder="Codigo" aria-label="Email"
                                aria-describedby="email-addon">
                        </div>
                        <div class="text-center">
                            <a href="javascript:;" class="btn bg-gradient-primary w-100 mt-4 mb-0">Restablecer
                                contraseña</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6"
                    style="background-image:url({{ url('img/login.png') }})"></div>
            </div>
        </div>
    </div>
@endsection
