@extends('layouts.app')
@section('content')
    <div class="row-reverse">
        <div class="col my-5">
            <div class="row">
                <div class="col">
                    <label for="" class="h3">Clientes</label>
                </div>
                <div class="col d-flex justify-content-end">
                    <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar
                        Usuario</a>
                </div>
            </div>
        </div>
        <div class="col">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th scope="col">Avatar</th>
                        <th scope="col">Nombre completo</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Puntos acumulados</th>
                        <th scope="col">Tipo de usuario</th>
                        <th scope="col">Fecha de registro</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col d-flex justify-content-center">
                                        <div class="img_user">
                                            <img src="{{ url('img/user2.png') }}" alt="" class="w-100">
                                        </div>
                                    </div>
                                </div>
                            </td>


                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->total_points }}</td>
                            <td>
                                @if ($user->role_id == 1)
                                    Cliente
                                @else
                                    Profesional
                                @endif
                            </td>
                            <td>{{ $user->created_at }}</td>
                            <td>
                                <div class="row ">
                                    <div class="col d-flex justify-content-center ">
                                        <a href="{{ route('user.client_edit') }}" class="btn btn-primary w-50"
                                            data-bs-toggle="modal" data-bs-target="#exampleModal2"><i
                                                class="fas fa-edit"></i></a>
                                    </div>
                                    <div class="col d-flex justify-content-center ">
                                        <a href="javascript:;" class="btn btn-primary w-50"><i
                                                class="fas fa-trash"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>


        </div>
    </div>

    <!-- Modal Crear-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear usuario (Cliente)</h5>
                    <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body py-3">
                    <div class="row">
                        <div class="col">
                            <form action="{{ route('user.new') }}" method="post" id="register">
                                @csrf
                                <div class="row-reverse">
                                    <div class="col mb-3">
                                        <input type="text" name="full_name" id="" class="form-control"
                                            placeholder="Nombre completo" required>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <input type="email" name="email" id="" class="form-control"
                                                    placeholder="Correo electronico">
                                            </div>
                                            <div class="col">
                                                <input type="password" name="password" id="" class="form-control"
                                                    placeholder="Contraseña (opcional)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <select name="active" id="" class="form-control">
                                                    <option value="" disabled selected>Seleccione el estado...</option>
                                                    <option value="1">Activo</option>
                                                    <option value="0">Inactivo</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <select name="role_id" id="" class="form-control">
                                                    <option value="" disabled selected>Seleccione el rol...</option>
                                                    <option value="1">Paciente</option>
                                                    <option value="2">Profesional</option>
                                                    <option value="3">Demo (Usuarios de prueba)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <input type="text" name="address" id="" class="form-control"
                                                    placeholder="Direccion" required>
                                            </div>
                                            <div class="col">
                                                <input type="text" name="phone" id="" class="form-control"
                                                    placeholder="Telefono" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <label for="">Imagen de perfil</label>
                                                <input type="file" name="avatar" id="" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center ">
                    <button type="submit" form="register" href="javascript:;"
                        class="btn w-50 bg-gradient-primary">REGISTRAR</a>
                </div>
            </div>
        </div>
    </div>



    <style>
        .img_user {
            width: 100px;
            height: 100px;
            background: gray;
            border-radius: 60px;
        }

    </style>
@endsection
