@extends('layouts.app')
@section('content')
    <div class="row-reverse">

        <div class="col card card-body">
            <div class="col my-5">
                <div class="row">
                    <div class="col">
                        <label for="" class="h3">Editar cliente</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <form action="" method="post">
                        <div class="row-reverse">
                            <div class="col mb-3">
                                <input type="text" name="full_name" id="" class="form-control"
                                    placeholder="Nombre completo" required>
                            </div>
                            <div class="col mb-3">
                                <div class="row">
                                    <div class="col">
                                        <input type="email" name="email" id="" class="form-control"
                                            placeholder="Correo electronico">
                                    </div>
                                    <div class="col">
                                        <input type="password" name="password" id="" class="form-control"
                                            placeholder="Contraseña (opcional)">
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div class="row">
                                    <div class="col">
                                        <select name="active" id="" class="form-control">
                                            <option value="" disabled selected>Seleccione el estado...</option>
                                            <option value="1">Activo</option>
                                            <option value="0">Inactivo</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select name="role_id" id="" class="form-control">
                                            <option value="" disabled selected>Seleccione el rol...</option>
                                            <option value="1">Paciente</option>
                                            <option value="2">Profesional</option>
                                            <option value="3">Demo (Usuarios de prueba)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div class="row">
                                    <div class="col">
                                        <input type="text" name="address" id="" class="form-control"
                                            placeholder="Direccion" required>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="phone" id="" class="form-control" placeholder="Telefono"
                                            required>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div class="row">
                                    <div class="col">
                                        <label for="">Imagen de perfil</label>
                                        <input type="file" name="avatar" id="" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <a href="" class="btn btn-primary w-25">ACTUALIZAR</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </div>
@endsection
