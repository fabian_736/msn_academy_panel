@extends('layouts.app')
@section('content')
    <div class="row-reverse">
        <div class="col my-5">
            <div class="row">
                <div class="col">
                    <label for="" class="h3">Administradores</label>
                </div>
                <div class="col d-flex justify-content-end">
                    <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar
                        Usuario</a>
                </div>
            </div>
        </div>
        <div class="col">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th scope="col">Nombre completo</th>
                        <th scope="col">Correo electronico</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Rol</th>
                        <th scope="col">Ultima sesion</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Mark</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>
                            <div class="row ">
                                <div class="col d-flex justify-content-center ">
                                    <a href="{{ route('user.admin_edit') }}" class="btn btn-primary w-50"
                                        data-bs-toggle="modal" data-bs-target="#exampleModal2"><i
                                            class="fas fa-edit"></i></a>
                                </div>
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Mark</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>
                            <div class="row ">
                                <div class="col d-flex justify-content-center ">
                                    <a href="{{ route('user.admin_edit') }}" class="btn btn-primary w-50"
                                        data-bs-toggle="modal" data-bs-target="#exampleModal2"><i
                                            class="fas fa-edit"></i></a>
                                </div>
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Mark</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>
                            <div class="row ">
                                <div class="col d-flex justify-content-center ">
                                    <a href="{{ route('user.admin_edit') }}" class="btn btn-primary w-50"
                                        data-bs-toggle="modal" data-bs-target="#exampleModal2"><i
                                            class="fas fa-edit"></i></a>
                                </div>
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>


        </div>
    </div>

    <!-- Modal Crear-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear usuario (Administrador)</h5>
                    <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body py-3">
                    <div class="row">
                        <div class="col">
                            <form action="" method="post">
                                <div class="row-reverse">
                                    <div class="col mb-3">
                                        <input type="text" name="" id="" class="form-control"
                                            placeholder="Nombre completo">
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <input type="text" name="" id="" class="form-control"
                                                    placeholder="Correo electronico">
                                            </div>
                                            <div class="col">
                                                <input type="text" name="" id="" class="form-control"
                                                    placeholder="Contraseña (opcional)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <select name="" id="" class="form-control">
                                                    <option value="" disabled selected>Seleccione el estado...</option>
                                                    <option value="">Activo</option>
                                                    <option value="">Inactivo</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <select name="" id="" class="form-control">
                                                    <option value="" disabled selected>Seleccione el rol...</option>
                                                    <option value="">SuperAdmin</option>
                                                    <option value="">Call Center</option>
                                                    <option value="">Basico</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center ">
                    <a href="javascript:;" class="btn w-50 bg-gradient-primary">REGISTRAR</a>
                </div>
            </div>
        </div>
    </div>
@endsection
