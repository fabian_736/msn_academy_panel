@extends('layouts.app')
@section('content')
    <div class="row-reverse">

        <div class="col card card-body">
            <div class="col my-5">
                <div class="row">
                    <div class="col">
                        <label for="" class="h3">Editar Administradores</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <form action="" method="post">
                        <div class="row-reverse">
                            <div class="col mb-3">
                                <input type="text" name="" id="" class="form-control" placeholder="Nombre completo"
                                    value="Super admin">
                            </div>
                            <div class="col mb-3">
                                <div class="row">
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control"
                                            placeholder="Correo electronico" value="admin@admin.com">
                                    </div>
                                    <div class="col">
                                        <input type="text" name="" id="" class="form-control"
                                            placeholder="Contraseña (opcional)" value="*********">
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div class="row">
                                    <div class="col">
                                        <select name="" id="" class="form-control">
                                            <option value="" disabled selected>Seleccione el estado...</option>
                                            <option value="">Activo</option>
                                            <option value="">Inactivo</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select name="" id="" class="form-control">
                                            <option value="" disabled selected>Seleccione el rol...</option>
                                            <option value="">SuperAdmin</option>
                                            <option value="">Call Center</option>
                                            <option value="">Basico</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col d-flex justify-content-center my-3">
                                <a href="" class="btn btn-primary w-25">ACTUALIZAR</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </div>
@endsection
