<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('img/logo_login.png')}}">
    <link rel="icon" type="image/png" href="{{url('img/logo_login.png')}}">
    <title>
        MSN Academy | Panel Administrativo
    </title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link id="pagestyle" href="{{ url('css/app.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/solid.css"
        integrity="sha384-Tv5i09RULyHKMwX0E8wJUqSOaXlyu3SQxORObAI08iUwIalMmN5L6AvlPX2LMoSE" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/fontawesome.css"
        integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous" />

    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="{{url('datatable/datatables/datatables.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('datatable/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}">


    
</head>

<body class="g-sidenav-show  bg-gray-100">
