@include('layouts.components.head')

@include('layouts.components.sidebar')

<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">

    @include('layouts.components.navbar')

    <div class="container-fluid py-4">

      @yield('content')

        {{-- @include('layouts.components.footer') --}}

    </div>

</main>

{{-- @include('layouts.components.paint') --}}

@include('layouts.components.end')
