@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header min-height-300 border-radius-xl mt-4"
            style="background-image: url({{ url('img/login.png') }}); background-position-y: 50%;">
            <span class="mask bg-gradient-primary opacity-6"></span>
        </div>
        <div class="card card-body blur shadow-blur mx-4 mt-n6 overflow-hidden">
            <div class="row gx-4">
                <div class="col-auto">
                    <div class="avatar avatar-xl position-relative">
                        <img src="{{ url('img/user2.png') }}" alt="profile_image" class="w-100 border-radius-lg shadow-sm">
                    </div>
                </div>
                <div class="col-auto my-auto">
                    <div class="h-100">
                        <h5 class="mb-1">
                            Usuario prueba
                        </h5>
                        <p class="mb-0 font-weight-bold text-sm">
                            Super Admin
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
                    <div class="nav-wrapper position-relative end-0">
                        <ul class="nav nav-pills nav-fill p-1 bg-transparent" role="tablist">
                            <li class="nav-item">
                                <img src="{{ url('img/logo/1.png') }}" alt="" class="w-50">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12 col-xl-8">
                <div class="card h-100">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-md-8 d-flex align-items-center">
                                <h6 class="mb-0">Informacion del perfil</h6>
                            </div>
                            <div class="col-md-4 text-end">
                                <a href="javascript:;" onclick="edit()" class="edit">
                                    <i class="fas fa-user-edit text-secondary text-sm" title="Edit Profile"></i>
                                </a>
                                <a href="javascript:;" onclick="confirm()" style="display: none" class="confirm">
                                    <i class="fas fa-check-circle text-secondary text-md" title="Confirm"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-3">
                        <textarea name="" id="input-1" cols="30" rows="5" class="form-control"
                            placeholder="Escribe la informacion de tu perfil..." style="display: none">Hi, I’m Alec Thompson, Decisions: If you can’t decide, the answer is no. If two equally difficult paths, choose the one more painful in the short term (pain avoidance is creating an illusion of equality).
                        </textarea>
                        <p class="text-sm" id="print-1">
                            Hi, I’m Alec Thompson, Decisions: If you can’t decide, the answer is no. If two equally
                            difficult paths, choose the one more painful in the short term (pain avoidance is creating an
                            illusion of equality).
                        </p>
                        <hr class="horizontal gray-light my-4">
                        <ul class="list-group">
                            <li class="list-group-item border-0 ps-0 pt-0 text-sm">
                                <div class="row">
                                    <div class="col-2 d-flex justify-content-end align-items-center">
                                        <strong class="text-dark">Nombre
                                            completo:</strong>
                                    </div>
                                    <div class="col mt-3">
                                        <p id="print-2"> Alec M. Thompson</p>
                                        <input type="text" name="" id="input-2" class="form-control"
                                            placeholder="Nombre completo" value="Alec M. Thompson" style="display: none">
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item border-0 ps-0 text-sm">
                                <div class="row">
                                    <div class="col-2 d-flex justify-content-end align-items-center">
                                        <strong class="text-dark">Telefono:</strong>
                                    </div>
                                    <div class="col mt-3">
                                        <p id="print-3"> (44) 123 1234 123 </p>
                                        <input type="number" name="" id="input-3" class="form-control"
                                            placeholder="Telefono" value="23424" style="display: none">
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item border-0 ps-0 text-sm">
                                <div class="row">
                                    <div class="col-2 d-flex justify-content-end align-items-center">
                                        <strong class="text-dark">Correo:</strong>
                                    </div>
                                    <div class="col mt-3">
                                        <p id="print-4"> alecthompson@mail.com </p>
                                        <input type="email" name="" value="alecthompson@mail.com" id="input-4"
                                            class="form-control" placeholder="Correo electronico" style="display: none">
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item border-0 ps-0 text-sm">
                                <div class="row">
                                    <div class="col-2 d-flex justify-content-end align-items-center">
                                        <strong class="text-dark">Ubicacion:</strong>
                                    </div>
                                    <div class="col mt-3">
                                        <p id="print-5"> USA </p>
                                        <select name="" id="input-5" style="display: none" class="form-control">
                                            <option value="" selected disabled></option>
                                            <option value="">Colombia</option>
                                            <option value="">Perú</option>
                                        </select>
                                    </div>
                                </div>
                            </li>


                            <li class="list-group-item border-0 ps-0 pb-0">
                                <div class="row">
                                    <div class="col-2 d-flex justify-content-end align-items-center">
                                        <strong class="text-dark">Social:</strong>
                                    </div>
                                    <div class="col">
                                        <a class="btn btn-facebook btn-simple mb-0 ps-1 pe-2 py-0" href="javascript:;">
                                            <i class="fab fa-facebook fa-lg"></i>
                                        </a>
                                        <a class="btn btn-twitter btn-simple mb-0 ps-1 pe-2 py-0" href="javascript:;">
                                            <i class="fab fa-twitter fa-lg"></i>
                                        </a>
                                        <a class="btn btn-instagram btn-simple mb-0 ps-1 pe-2 py-0" href="javascript:;">
                                            <i class="fab fa-instagram fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>

                            <li class="bottom_update" style="display: none">
                                <div class="row">
                                    <div class="col d-flex justify-content-center">
                                        <button class="btn btn-primary">ACTUALIZAR</button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-4">
                <div class="card h-100">
                    <div class="card-header pb-0 p-3">
                        <h6 class="mb-0">Ultimas Accciones</h6>
                    </div>
                    <div class="card-body p-3">
                        <ul class="list-group">
                            <li class="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                                <div class="avatar me-3">
                                    <img src="../assets/img/kal-visuals-square.jpg" alt="kal"
                                        class="border-radius-lg shadow">
                                </div>
                                <div class="d-flex align-items-start flex-column justify-content-center">
                                    <h6 class="mb-0 text-sm">Sophie B.</h6>
                                    <p class="mb-0 text-xs">Hi! I need more information..</p>
                                </div>
                                <a class="btn btn-link pe-3 ps-0 mb-0 ms-auto" href="javascript:;">Reply</a>
                            </li>
                            <li class="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                                <div class="avatar me-3">
                                    <img src="../assets/img/marie.jpg" alt="kal" class="border-radius-lg shadow">
                                </div>
                                <div class="d-flex align-items-start flex-column justify-content-center">
                                    <h6 class="mb-0 text-sm">Anne Marie</h6>
                                    <p class="mb-0 text-xs">Awesome work, can you..</p>
                                </div>
                                <a class="btn btn-link pe-3 ps-0 mb-0 ms-auto" href="javascript:;">Reply</a>
                            </li>
                            <li class="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                                <div class="avatar me-3">
                                    <img src="../assets/img/ivana-square.jpg" alt="kal" class="border-radius-lg shadow">
                                </div>
                                <div class="d-flex align-items-start flex-column justify-content-center">
                                    <h6 class="mb-0 text-sm">Ivanna</h6>
                                    <p class="mb-0 text-xs">About files I can..</p>
                                </div>
                                <a class="btn btn-link pe-3 ps-0 mb-0 ms-auto" href="javascript:;">Reply</a>
                            </li>
                            <li class="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                                <div class="avatar me-3">
                                    <img src="../assets/img/team-4.jpg" alt="kal" class="border-radius-lg shadow">
                                </div>
                                <div class="d-flex align-items-start flex-column justify-content-center">
                                    <h6 class="mb-0 text-sm">Peterson</h6>
                                    <p class="mb-0 text-xs">Have a great afternoon..</p>
                                </div>
                                <a class="btn btn-link pe-3 ps-0 mb-0 ms-auto" href="javascript:;">Reply</a>
                            </li>
                            <li class="list-group-item border-0 d-flex align-items-center px-0">
                                <div class="avatar me-3">
                                    <img src="../assets/img/team-3.jpg" alt="kal" class="border-radius-lg shadow">
                                </div>
                                <div class="d-flex align-items-start flex-column justify-content-center">
                                    <h6 class="mb-0 text-sm">Nick Daniel</h6>
                                    <p class="mb-0 text-xs">Hi! I need more information..</p>
                                </div>
                                <a class="btn btn-link pe-3 ps-0 mb-0 ms-auto" href="javascript:;">Reply</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        const edit = () => {
            $(".edit").hide('slow');
            $(".confirm").show('slow');

            $("#input-1").show('slow');
            $("#print-1").hide('slow');

            $("#input-2").show('slow');
            $("#print-2").hide('slow');

            $("#input-3").show('slow');
            $("#print-3").hide('slow');

            $("#input-4").show('slow');
            $("#print-4").hide('slow');

            $("#input-5").show('slow');
            $("#print-5").hide('slow');

            $(".bottom_update").show('slow');
        };

        const confirm = () => {
            $(".edit").show('slow');
            $(".confirm").hide('slow');

            $("#input-1").hide('slow');
            $("#print-1").show('slow');

            $("#input-2").hide('slow');
            $("#print-2").show('slow');

            $("#input-3").hide('slow');
            $("#print-3").show('slow');

            $("#input-4").hide('slow');
            $("#print-4").show('slow');

            $("#input-5").hide('slow');
            $("#print-5").show('slow');

            $(".bottom_update").hide('slow');

        };
    </script>
@endsection
