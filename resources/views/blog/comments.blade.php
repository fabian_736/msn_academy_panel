@extends('layouts.app')
@section('content')
    <div class="row-reverse">
        <div class="col my-5">
            <div class="row">
                <div class="col">
                    <label for="" class="h3">Comentarios</label>
                </div>
            </div>
        </div>
        <div class="col">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th scope="col">Responsable</th>
                        <th scope="col">Fecha de creacion</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Ultima sesion</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Mark</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>
                            <div class="row ">
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Mark</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>
                            <div class="row ">
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Mark</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>
                            <div class="row ">
                                <div class="col d-flex justify-content-center ">
                                    <a href="javascript:;" class="btn btn-primary w-50"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>


        </div>
    </div>
@endsection
