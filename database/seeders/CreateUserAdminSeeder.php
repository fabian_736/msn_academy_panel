<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use DB;

class CreateUserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'username' => 'admin1',
                'password' => bcrypt('password'),
                'uuid' => Str::uuid(),

                'email' => 'admin1@people.com',
                'name' => 'Administrador',
                'created_at' => now(),
            ],
            [
                'username' => 'admin2',
                'password' => bcrypt('password'),
                'uuid' => Str::uuid(),

                'email' => 'admin2@people.com',
                'name' => 'Administrador2',
                'created_at' => now(),

            ]
        ];

        foreach ($users as $user) {
            DB::table('admins')->insert($user);
        }
    }
}
